import 'package:flutter/material.dart';

class ListHeader extends StatelessWidget {
  final String title;
  const ListHeader({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 20.0,
              horizontal: 80,
            ),
            child: Text(
              title,
              style: titleTextStyle,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40.0),
            child: Container(
              height: 1,
              color: Colors.black12,
            ),
          ),
        ],
      ),
    );
  }
}

TextStyle titleTextStyle = TextStyle(
  fontSize: 30,
  color: Colors.black54,
);
