import 'package:flutter/material.dart';
import 'package:sapio_admin/data_types/user_dataType.dart';
import 'package:sapio_admin/elements/usuario_form.dart';

import 'asign_user_form.dart';

class UserListTileElement extends StatelessWidget {
  final User usuario;
  final String type;
  const UserListTileElement({
    Key key,
    @required this.usuario,
    @required this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        constraints: BoxConstraints(maxWidth: 600, minHeight: 70),
        margin: EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 30,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 3,
              blurRadius: 5,
              offset: Offset(0, 1),
            ),
          ],
        ),
        child: Row(
          children: [
            Container(
              constraints: BoxConstraints(minHeight: 70),
              width: 10,
              decoration: BoxDecoration(
                color: Colors.amberAccent,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  bottomLeft: Radius.circular(8),
                ),
              ),
            ),
            SizedBox(width: 20),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    usuario.nombre,
                    style: titleTextStyle,
                  ),
                  Text(
                    usuario.mail,
                    style: descriptionTextStyle,
                  ),
                ],
              ),
            ),
            Visibility(
              visible: type != "administradores",
              child: IconButton(
                icon: Icon(
                  Icons.assignment_turned_in_outlined,
                  size: 28,
                  color: Colors.grey,
                ),
                tooltip: 'Asignar Cursos',
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          content: AssignationUserForm(
                            usuario: usuario,
                          ),
                        );
                      });
                },
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.more_vert,
                size: 30,
                color: Colors.grey,
              ),
              tooltip: 'Ver usuario',
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        content: UeserForm(
                          type: type,
                          usuario: usuario,
                        ),
                      );
                    });
              },
            ),
            SizedBox(width: 20),
          ],
        ),
      ),
    );
  }
}

TextStyle titleTextStyle = TextStyle(
  fontSize: 25,
  color: Colors.black54,
  shadows: [
    Shadow(offset: Offset(2.0, 3.0), blurRadius: 3.0, color: Colors.black12)
  ],
);

TextStyle descriptionTextStyle = TextStyle(
  fontSize: 16,
  color: Colors.black54,
);
