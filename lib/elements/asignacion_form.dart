import 'package:flutter/material.dart';
import 'package:sapio_admin/base_app/api_client.dart';

class AsignacionForm extends StatefulWidget {
  final String type;
  AsignacionForm({Key key, @required this.type}) : super(key: key);

  @override
  _AsignacionFormState createState() => _AsignacionFormState();
}

class _AsignacionFormState extends State<AsignacionForm> {
  final _formKey = GlobalKey<FormState>();
  int _value = 1;
  bool isWaiting = false;
  final usuarioController = TextEditingController();
  final cursoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        child: Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xFFFB6107),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Asignar ${widget.type} a curso',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: TextFormField(
                    controller: cursoController,
                    decoration: InputDecoration(labelText: 'Id curso'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: TextFormField(
                    controller: usuarioController,
                    decoration: InputDecoration(labelText: 'Mail usuario'),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Cancelar'),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.grey[200])),
                    ),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() {
                          isWaiting = true;
                        });
                        if (_formKey.currentState.validate()) {
                          var client = ApiClient();
                          var valid = false;
                          valid = await client.enrollKnownUser(
                              userMail: usuarioController.text,
                              courseId: cursoController.text);
                          if (valid) {}
                        }
                        setState(() {
                          isWaiting = false;
                        });
                      },
                      child: Text('Enviar'),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.greenAccent)),
                    ),
                  ],
                ),
              ],
            ),
            Positioned.fill(
              child: Visibility(
                visible: isWaiting,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
