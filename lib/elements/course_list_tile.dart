import 'package:flutter/material.dart';
import 'package:sapio_admin/data_types/course_dataType.dart';
import 'package:sapio_admin/elements/content_form.dart';
import 'package:sapio_admin/pages/course_page.dart';

import 'curso_form.dart';

class CourseListTileElement extends StatelessWidget {
  final Function(Widget) onElementSelected;
  final Course curso;
  const CourseListTileElement({
    Key key,
    @required this.curso,
    @required this.onElementSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        constraints: BoxConstraints(maxWidth: 600, minHeight: 70),
        margin: EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 30,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 3,
              blurRadius: 5,
              offset: Offset(0, 1),
            ),
          ],
        ),
        child: Row(
          children: [
            Container(
              constraints: BoxConstraints(minHeight: 70),
              width: 10,
              decoration: BoxDecoration(
                color: Colors.amberAccent,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  bottomLeft: Radius.circular(8),
                ),
              ),
            ),
            SizedBox(width: 20),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    curso.nombre,
                    style: titleTextStyle,
                  ),
                  Text(
                    curso.descripcion,
                    style: descriptionTextStyle,
                  ),
                ],
              ),
            ),
            IconButton(
              onPressed: () {
                CoursePage cp = new CoursePage(
                  curso: curso,
                  onElementSelected: (Widget val) => onElementSelected(val),
                );
                onElementSelected(cp);
              },
              padding: const EdgeInsets.only(right: 4.0, top: 6),
              icon: Icon(
                Icons.remove_red_eye,
                size: 30,
                color: Colors.grey,
              ),
              tooltip: 'Ver Curso',
            ),
            IconButton(
              icon: Icon(
                Icons.more_vert,
                size: 30,
                color: Colors.grey,
              ),
              tooltip: 'Editar',
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        content: CursoForm(
                          curso: curso,
                        ),
                      );
                    });
              },
            ),
            SizedBox(width: 20),
          ],
        ),
      ),
    );
  }
}

TextStyle titleTextStyle = TextStyle(
  fontSize: 25,
  color: Colors.black54,
  shadows: [
    Shadow(offset: Offset(2.0, 3.0), blurRadius: 3.0, color: Colors.black12)
  ],
);

TextStyle descriptionTextStyle = TextStyle(
  fontSize: 16,
  color: Colors.black54,
);
