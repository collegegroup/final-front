import 'package:flutter/material.dart';
import 'package:sapio_admin/base_app/api_client.dart';
import 'package:sapio_admin/data_types/user_dataType.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

import 'change_pass_form.dart';

class UeserForm extends StatefulWidget {
  final String type;
  User usuario;
  UeserForm({
    Key key,
    @required this.type,
    this.usuario,
  }) : super(key: key);

  @override
  _UeserFormState createState() => _UeserFormState();
}

class _UeserFormState extends State<UeserForm> {
  final _formKey = GlobalKey<FormState>();
  String _value = 'ci';
  bool isWaiting = false;
  final mailController = TextEditingController();
  final nameController = TextEditingController();
  final directionContrller = TextEditingController();
  final careerController = TextEditingController();
  final passwordController = TextEditingController();
  final documentController = TextEditingController();

  bool isSubmitEnabled = true;
  bool isDeleteEnabled = true;

  @override
  void initState() {
    super.initState();
    if (widget.usuario != null) {
      mailController.text = widget.usuario.mail;
      nameController.text = widget.usuario.nombre;
      directionContrller.text = widget.usuario.direccion;
      careerController.text = widget.usuario.carrera;
      documentController.text = widget.usuario.documento;
      _value = widget.usuario.tipoDocumento.toLowerCase();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xFFFB6107),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Usuario - ${widget.type}',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: TextFormField(
                    controller: nameController,
                    decoration: InputDecoration(labelText: 'Nombre'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: TextFormField(
                    controller: mailController,
                    decoration: InputDecoration(labelText: 'Mail'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: TextFormField(
                    controller: directionContrller,
                    decoration: InputDecoration(labelText: 'Dirección'),
                  ),
                ),
                Visibility(
                  child: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: TextFormField(
                      controller: careerController,
                      decoration: InputDecoration(labelText: 'Carrera'),
                    ),
                  ),
                  visible: (widget.type == "estudiante"),
                ),
                Visibility(
                  child: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: TextFormField(
                      controller: passwordController,
                      obscureText: true,
                      decoration: InputDecoration(labelText: 'Contraseña'),
                    ),
                  ),
                  visible: (widget.type == "self"),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(3.0),
                        child: TextFormField(
                          controller: documentController,
                          decoration: InputDecoration(labelText: 'Documento'),
                        ),
                      ),
                    ),
                    DropdownButton(
                        value: _value,
                        items: [
                          DropdownMenuItem(child: Text("CI"), value: 'ci'),
                          DropdownMenuItem(
                              child: Text("Pasaporte"), value: 'pasaporte'),
                          DropdownMenuItem(child: Text("DNI"), value: 'dni'),
                        ],
                        onChanged: (value) {
                          setState(() {
                            _value = value;
                          });
                        }),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Cancelar',
                          style: TextStyle(color: Colors.black45)),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.grey[200])),
                    ),
                    Visibility(
                      child: ElevatedButton(
                        onPressed: () async {
                          setState(() {
                            isWaiting = true;
                          });
                          if (isDeleteEnabled) {
                            isDeleteEnabled = false;
                            bool valid = false;
                            var client = ApiClient();
                            valid = await client.delUser(user: widget.usuario);
                            if (valid) {
                              showToast(
                                  'El usuario ${widget.usuario.nombre} fue eliminado',
                                  context: context,
                                  animation:
                                      StyledToastAnimation.slideFromBottom,
                                  reverseAnimation:
                                      StyledToastAnimation.slideToBottom,
                                  startOffset: Offset(0.0, 3.0),
                                  reverseEndOffset: Offset(0.0, 3.0),
                                  position: StyledToastPosition.bottom,
                                  duration: Duration(seconds: 4),
                                  //Animation duration   animDuration * 2 <= duration
                                  animDuration: Duration(seconds: 1),
                                  curve: Curves.elasticOut,
                                  reverseCurve: Curves.fastOutSlowIn);
                              Navigator.of(context).pop();
                            } else {
                              showToast('Error',
                                  context: context,
                                  animation:
                                      StyledToastAnimation.slideFromBottom,
                                  reverseAnimation:
                                      StyledToastAnimation.slideToBottom,
                                  startOffset: Offset(0.0, 3.0),
                                  reverseEndOffset: Offset(0.0, 3.0),
                                  position: StyledToastPosition.bottom,
                                  duration: Duration(seconds: 4),
                                  //Animation duration   animDuration * 2 <= duration
                                  animDuration: Duration(seconds: 1),
                                  curve: Curves.elasticOut,
                                  reverseCurve: Curves.fastOutSlowIn);
                            }
                            isDeleteEnabled = true;
                          }
                          setState(() {
                            isWaiting = false;
                          });
                        },
                        child: Text('Eliminar'),
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.redAccent)),
                      ),
                      visible: (widget.usuario != null),
                    ),
                    ElevatedButton(
                      onPressed: () async {
                        setState(() {
                          isWaiting = true;
                        });
                        if (_formKey.currentState.validate() &&
                            isSubmitEnabled) {
                          isSubmitEnabled = false;
                          User nuevo_usuario = User(
                            nombre: nameController.text,
                            mail: mailController.text,
                            direccion: directionContrller.text,
                            carrera: careerController.text,
                            documento: documentController.text,
                            tipoDocumento: _value,
                          );
                          bool valid = false;
                          var client = ApiClient();
                          if (widget.usuario != null) {
                            if (widget.type == "self") {
                              valid = await client.updSelfUser(
                                  user: nuevo_usuario,
                                  pass: passwordController.text);
                            } else {
                              valid = await client.updUser(user: nuevo_usuario);
                            }

                            if (valid) {
                              showToast(
                                  'El usuario ${nuevo_usuario.nombre} fue editado correctamente',
                                  context: context,
                                  animation:
                                      StyledToastAnimation.slideFromBottom,
                                  reverseAnimation:
                                      StyledToastAnimation.slideToBottom,
                                  startOffset: Offset(0.0, 3.0),
                                  reverseEndOffset: Offset(0.0, 3.0),
                                  position: StyledToastPosition.bottom,
                                  duration: Duration(seconds: 4),
                                  //Animation duration   animDuration * 2 <= duration
                                  animDuration: Duration(seconds: 1),
                                  curve: Curves.elasticOut,
                                  reverseCurve: Curves.fastOutSlowIn);
                              Navigator.of(context).pop();
                            } else {
                              showToast('Error al editar el usuario',
                                  context: context,
                                  animation:
                                      StyledToastAnimation.slideFromBottom,
                                  reverseAnimation:
                                      StyledToastAnimation.slideToBottom,
                                  startOffset: Offset(0.0, 3.0),
                                  reverseEndOffset: Offset(0.0, 3.0),
                                  position: StyledToastPosition.bottom,
                                  duration: Duration(seconds: 4),
                                  //Animation duration   animDuration * 2 <= duration
                                  animDuration: Duration(seconds: 1),
                                  curve: Curves.elasticOut,
                                  reverseCurve: Curves.fastOutSlowIn);
                            }
                          } else {
                            switch (widget.type) {
                              case 'estudiante':
                                valid = await client.addStudent(
                                    user: nuevo_usuario);
                                break;
                              case 'docente':
                                valid = await client.addTeacher(
                                    user: nuevo_usuario);
                                break;
                              case 'administrador':
                                valid =
                                    await client.addAdmin(user: nuevo_usuario);
                                break;
                              default:
                            }
                            if (valid) {
                              showToast(
                                  'El usuario ${nuevo_usuario.nombre} del tipo ${widget.type} fue creado correctamente',
                                  context: context,
                                  animation:
                                      StyledToastAnimation.slideFromBottom,
                                  reverseAnimation:
                                      StyledToastAnimation.slideToBottom,
                                  startOffset: Offset(0.0, 3.0),
                                  reverseEndOffset: Offset(0.0, 3.0),
                                  position: StyledToastPosition.bottom,
                                  duration: Duration(seconds: 4),
                                  //Animation duration   animDuration * 2 <= duration
                                  animDuration: Duration(seconds: 1),
                                  curve: Curves.elasticOut,
                                  reverseCurve: Curves.fastOutSlowIn);
                              Navigator.of(context).pop();
                            } else {
                              showToast('Error al crear el usuario',
                                  context: context,
                                  animation:
                                      StyledToastAnimation.slideFromBottom,
                                  reverseAnimation:
                                      StyledToastAnimation.slideToBottom,
                                  startOffset: Offset(0.0, 3.0),
                                  reverseEndOffset: Offset(0.0, 3.0),
                                  position: StyledToastPosition.bottom,
                                  duration: Duration(seconds: 4),
                                  //Animation duration   animDuration * 2 <= duration
                                  animDuration: Duration(seconds: 1),
                                  curve: Curves.elasticOut,
                                  reverseCurve: Curves.fastOutSlowIn);
                            }
                          }
                          isSubmitEnabled = true;
                        }
                        setState(() {
                          isWaiting = false;
                        });
                      },
                      child: Text('Enviar'),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.greenAccent)),
                    ),
                  ],
                ),
                Visibility(
                  child: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: ElevatedButton(
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                  content: Container(
                                constraints: BoxConstraints(
                                  maxHeight: 700,
                                  maxWidth: 500,
                                  minHeight: 200,
                                  minWidth: 200,
                                ),
                                child: ChangePassFormm(),
                              ));
                            });
                      },
                      child: Text('Modificar contraseña',
                          style: TextStyle(color: Colors.white)),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.orangeAccent[200])),
                    ),
                  ),
                  visible: (widget.type == "self"),
                ),
              ],
            ),
          ),
        ),
        Positioned.fill(
          child: Visibility(
            visible: isWaiting,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        ),
      ],
    );
  }
}
