import 'package:flutter/material.dart';
import 'package:sapio_admin/base_app/api_client.dart';
import 'package:sapio_admin/data_types/course_dataType.dart';
import 'package:sapio_admin/data_types/user_dataType.dart';

class CourseAssignationTile extends StatefulWidget {
  final User usuario;
  final Course curso;
  bool isAssigned;
  CourseAssignationTile({Key key, this.usuario, this.curso, this.isAssigned})
      : super(key: key);

  @override
  _CourseAssignationTileState createState() => _CourseAssignationTileState();
}

class _CourseAssignationTileState extends State<CourseAssignationTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: 600, minHeight: 70),
      child: Row(
        children: [
          Expanded(child: Container(child: Text(widget.curso.nombre))),
          Visibility(
            visible: !widget.isAssigned,
            child: ElevatedButton(
              onPressed: () async {
                var client = ApiClient();
                var valid = false;
                valid = await client.enrollUser(
                    user: widget.usuario, course: widget.curso);
                if (valid) {
                  setState(() {
                    widget.isAssigned = true;
                  });
                }
              },
              child: Text('Asignar'),
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.greenAccent)),
            ),
          ),
          Visibility(
            visible: widget.isAssigned,
            child: ElevatedButton(
              onPressed: () async {
                var client = ApiClient();
                var valid = false;
                valid = await client.deEnrollUser(
                    user: widget.usuario, course: widget.curso);
                if (valid) {
                  setState(() {
                    widget.isAssigned = false;
                  });
                }
              },
              child: Text('Desasignar'),
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.redAccent)),
            ),
          ),
        ],
      ),
    );
    ;
  }
}
