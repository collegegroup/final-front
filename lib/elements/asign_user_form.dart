import 'package:flutter/material.dart';
import 'package:sapio_admin/base_app/api_client.dart';
import 'package:sapio_admin/data_types/course_dataType.dart';
import 'package:sapio_admin/data_types/user_dataType.dart';

import 'asignar_usuario.dart';

class AssignationUserForm extends StatelessWidget {
  final User usuario;
  const AssignationUserForm({Key key, this.usuario}) : super(key: key);

// Future<Map<Course, bool>>
  @override
  Widget build(BuildContext context) {
    var client = ApiClient();
    return Container(
      height: 600,
      width: 400,
      child: FutureBuilder<List<CourseEnrollment>>(
        future: client.getCoursesByUser(usuario: usuario),
        builder: (BuildContext context,
            AsyncSnapshot<List<CourseEnrollment>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return CourseAssignationTile(
                    usuario: usuario,
                    curso: snapshot.data[index].curso,
                    isAssigned: snapshot.data[index].isEnrolled,
                  );
                });
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
