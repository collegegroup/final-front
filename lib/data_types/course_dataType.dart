import 'package:sapio_admin/data_types/user_dataType.dart';

class Course {
  int id;
  String nombre;
  String descripcion;
  String color;
  int creditos;
  String fechaInicio;

  Course({
    this.creditos,
    this.descripcion,
    this.id,
    this.color,
    this.nombre,
    this.fechaInicio,
  });

  Course.fromJson(Map<String, dynamic> json)
      : id = json['_id'],
        nombre = json['nombre'],
        creditos = json['creditos'],
        color = json['color'],
        fechaInicio = json['fechaInicio'],
        descripcion = json['descripcion'];

  Map<String, dynamic> toJson() => {
        'nombre': nombre,
        'descripcion': descripcion,
        'creditos': creditos,
        'color': color,
        'fechaInicio': fechaInicio,
      };

  Map<String, dynamic> toNestedJson() => {
        "Curso": {
          'nombre': nombre,
          'color': color,
          'descripcion': descripcion,
          'creditos': creditos,
          'fechaInicio': fechaInicio,
        }
      };
}

Course courseRelleno = Course(
  id: 4534534,
  nombre: "Programacion",
  creditos: 12,
  color: "",
  descripcion: "Et quo suscipit distinctio libero.",
);

class CourseEnrollment {
  Course curso;
  bool isEnrolled;
}
