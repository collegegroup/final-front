import 'package:flutter/material.dart';
import 'package:sapio_admin/pages/user_listing_page.dart';

import 'master_drawer.dart';

class MoblieLandingPage extends StatefulWidget {
  MoblieLandingPage({Key key}) : super(key: key);

  @override
  _MoblieLandingPageState createState() => _MoblieLandingPageState();
}

class _MoblieLandingPageState extends State<MoblieLandingPage> {
  Widget _body = UserListingPage(
    title: 'Estudiantes',
  );
  //Widget _body = CoursePage();
  _setBody(Widget val) {
    setState(() {
      _body = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    MasterDrawer md = new MasterDrawer(
      onElementSelected: (Widget val) => _setBody(val),
    );
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 4.0),
              child: Image.asset(
                'assets/images/LogoSappio.png',
                height: 40,
              ),
            ),
            Text('Sappio Admin'),
          ],
        ),
      ),
      drawer: Drawer(
        child: md,
      ),
      body: _body,
    );
  }

  static onElementSelected(Widget val) {}
}
