import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sapio_admin/base_app/user_credentials_data_type.dart';
import 'package:sapio_admin/pages/onboarding_page.dart';
import 'package:sapio_admin/pages/tabbed_login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'api_client.dart';
import 'desktop_landing_page.dart';
import 'mobile_landing_page.dart';

class BaseApp extends StatefulWidget {
  BaseApp({Key key}) : super(key: key);

  @override
  _BaseAppState createState() => _BaseAppState();
}

class _BaseAppState extends State<BaseApp> {
  TextEditingController nameController = TextEditingController();

  //@override
  //void initState() {
  //  super.initState();
  //}

  Future<Null> autoLogIn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String savedPreferencesString = prefs.getString("SappioAdmin");
    print(savedPreferencesString);
    if (savedPreferencesString == null || savedPreferencesString == '') {
      storedUserCredentials = emptyUser;
    } else {
      Map savedPreferences = jsonDecode(savedPreferencesString);
      storedUserCredentials = UserCredentials.fromJson(savedPreferences);
      if (storedUserCredentials.getUserData() == null) {
        storedUserCredentials = emptyUser;
      } else if (storedUserCredentials.getUserData().mail == '') {
        storedUserCredentials = emptyUser;
      } else {
        ApiClient.setAuthHeader();
        String title = "Sappio - Administrador";
        SystemChrome.setApplicationSwitcherDescription(
            ApplicationSwitcherDescription(
          label: title,
          primaryColor: Theme.of(context).primaryColor.value,
        ));
      }
    }
    return Future<Null>.sync(() => null);
  }

  Future<Null> logout() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("SappioAdmin", logedOffUser.toJson().toString());

    setState(() {
      storedUserCredentials = logedOffUser;
    });
  }

  Future<Null> loginUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("SappioAdmin", nameController.text);

    setState(() {
      storedUserCredentials.setName('newNickname');
      storedUserCredentials.setToken('newToken');
      storedUserCredentials.isNewUser = false;
    });
    String title = "Sappio - Administrador";
    SystemChrome.setApplicationSwitcherDescription(
        ApplicationSwitcherDescription(
      label: title,
      primaryColor: Theme.of(context).primaryColor.value,
    ));
    nameController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: autoLogIn(),
      builder: (BuildContext context, AsyncSnapshot<Null> snapshot) {
        //return DrawerLandingPage();
        if (storedUserCredentials == null) {
          return Container(
            color: Colors.blueGrey,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else {
          if (storedUserCredentials.userData.mail == null) {
            if (storedUserCredentials.isNewUser) {
              //* IF THE USER IS NEW
              return OnboardingPage();
            } else {
              //* IF IT IS NOT LOGGED IN, BUT NOT NEW
              return TabbedLoginPage();
            }
          } else {
            //* IF IT IS LOGGED IN
            return LandingPageLayoutBuilder();
          }
        }
      },
    );
  }
}

class LandingPageLayoutBuilder extends StatelessWidget {
  const LandingPageLayoutBuilder({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      if (constraints.maxWidth >= 900.0) {
        //* IF THE SCREENS WIDTH IS LARGER THAN 1100
        return DesktopLandingPage();
      }
      //* IF THE SCREENS WIDTH IS SMALLER THAN 650
      return MoblieLandingPage();
    });
  }
}
