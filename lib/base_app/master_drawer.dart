import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sapio_admin/base_app/upload_button.dart';
import 'package:sapio_admin/base_app/user_credentials_data_type.dart';
import 'package:sapio_admin/elements/asignacion_form.dart';
import 'package:sapio_admin/elements/curso_form.dart';
import 'package:sapio_admin/elements/usuario_form.dart';
import 'package:sapio_admin/pages/course_listing_page.dart';
import 'package:sapio_admin/pages/tabbed_login_page.dart';
import 'package:sapio_admin/pages/user_listing_page.dart';
import 'package:sapio_admin/tools/theme.dart';

class MasterDrawer extends StatelessWidget {
  final Function(Widget) onElementSelected;
  const MasterDrawer({
    Key key,
    @required this.onElementSelected,
  }) : super(key: key);

// AsignacionForm
  @override
  Widget build(BuildContext context) {
    SingleChildScrollView scsv = SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //* HEADER
          MasterHeader(
            onElementSelected: (Widget val) => onElementSelected(val),
          ),
          InkWell(
            onTap: () => onElementSelected(
              CourseListingPage(
                title: 'Cursos',
                onElementSelected: (Widget val) => onElementSelected(val),
              ),
            ),
            child: MasterTitle(
              text: "Cursos",
              icon: Icons.school,
            ),
          ),
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      content: CursoForm(),
                    );
                  });
            },
            child: MasterSubtitle(text: "Alta Curso"),
          ),
          InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        content: AsignacionForm(type: 'estudiante'),
                      );
                    });
              },
              child: MasterSubtitle(text: "Matricular Estudiante")),
          InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        content: AsignacionForm(type: 'docente'),
                      );
                    });
              },
              child: MasterSubtitle(text: "Matricular Docente")),

          InkWell(
            onTap: () => onElementSelected(
              UserListingPage(
                title: 'Estudiantes',
              ),
            ),
            child: MasterTitle(
              text: "Estudiantes",
              icon: Icons.message,
            ),
          ),
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      content: UeserForm(
                        type: 'estudiante',
                      ),
                    );
                  });
            },
            child: MasterSubtitle(text: "Alta Estudiante"),
          ),
          SizedBox(height: 60),

          InkWell(
            onTap: () => onElementSelected(
              UserListingPage(
                title: 'Docentes',
              ),
            ),
            child: MasterTitle(
              text: "Docentes",
              icon: Icons.assignment,
            ),
          ),
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      content: UeserForm(
                        type: 'docente',
                      ),
                    );
                  });
            },
            child: MasterSubtitle(text: "Alta Docente"),
          ),
          SizedBox(height: 60),

          InkWell(
            onTap: () => onElementSelected(
              UserListingPage(title: 'Administradores'),
            ),
            child: MasterTitle(
              text: "Administradores",
              icon: Icons.domain,
            ),
          ),
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      content: UeserForm(
                        type: 'administrador',
                      ),
                    );
                  });
            },
            child: MasterSubtitle(text: "Alta Administrador"),
          ),
          SizedBox(height: 60),

          SizedBox(height: 30),
          //* FOOTER
          MsterFooter(),
          SizedBox(height: 15),
        ],
      ),
    );
    return scsv;
  }
}

class MasterHeader extends StatelessWidget {
  final Function(Widget) onElementSelected;
  const MasterHeader({Key key, this.onElementSelected}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        //* BACKGROUND IMAGE
        Image.asset(
          'assets/images/background_mountains.jpg',
          width: 305,
          height: 250,
          fit: BoxFit.cover,
        ),
        Positioned(
          bottom: 60,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 10.0,
            ),
            child: Container(
              width: 280,
              child: Row(
                children: [
                  //* USER'S IMAGE
                  Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage(
                            "https://toppng.com/uploads/preview/instagram-default-profile-picture-11562973083brycehrmyv.png"),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    width: 175,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        //* USER'S NAME
                        Text(
                          storedUserCredentials.getNickname(),
                          overflow: TextOverflow.fade,
                          maxLines: 2,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                          ),
                        ),
                        //* USER'S TAG
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Text(
                            'Carrera',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: Colors.white70,
                              fontSize: 18,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        //* HOME BUTTON
        Positioned(
          bottom: 8,
          left: 100,
          child: FlatButton(
            color: Color(0xFFFB6107),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      content: AltaMasivaForm(),
                    );
                  });
            }, // => onElementSelected(UserListingPage()),
            child: Text(
              'Alta Masiva',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class MsterFooter extends StatelessWidget {
  const MsterFooter({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: [
        FlatButton(
          color: myAppTheme['InfoColor'],
          onPressed: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                      content: Container(
                    constraints: BoxConstraints(
                      maxHeight: 700,
                      maxWidth: 500,
                      minHeight: 200,
                      minWidth: 200,
                    ),
                    child: UeserForm(
                      usuario: storedUserCredentials.userData,
                      type: "self",
                    ),
                  ));
                });
          },
          child: Text(
            //* HELP BUTTONF
            'Info Usuario',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ), // UserForm
        ),
        FlatButton(
          //* LOG-OUT BUTTON
          color: Colors.black12,
          onPressed: () {
            //* LOG-OUT  BUTTON PRESSED POP-UP
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    content: Container(
                      height: 50,
                      child: Column(
                        children: [
                          Text('Seguro que quieres cerrar sesión?'),
                          Row(
                            children: [
                              FlatButton(
                                onPressed: () {},
                                child: Text('Cancelar'),
                              ),
                              FlatButton(
                                onPressed: () {
                                  String title = "Sappio - Login";
                                  SystemChrome
                                      .setApplicationSwitcherDescription(
                                          ApplicationSwitcherDescription(
                                    label: title,
                                    primaryColor:
                                        Theme.of(context).primaryColor.value,
                                  ));
                                  storedUserCredentials = logedOffUser;
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TabbedLoginPage(),
                                    ),
                                  );
                                },
                                child: Text('Si'),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                });
          },
          child: Text(
            'Cerrar Sesión',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ),
      ],
    );
  }
}

class MasterTitle extends StatelessWidget {
  final String text;
  final IconData icon;
  const MasterTitle({Key key, this.text, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 10,
          ),
          child: Row(
            children: [
              SizedBox(
                width: 30,
                height: 30,
              ),
              Icon(
                icon,
                color: Colors.grey[600],
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                text,
                overflow: TextOverflow.clip,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  color: Colors.grey[600],
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 1,
          margin: EdgeInsets.symmetric(
            horizontal: 30,
          ),
          color: Colors.black12,
        ),
      ],
    );
  }
}

class MasterSubtitle extends StatelessWidget {
  final String text;
  const MasterSubtitle({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 60,
          height: 40,
        ),
        Text(
          text,
          style: TextStyle(
            fontSize: 18,
            color: Colors.grey,
          ),
        ),
      ],
    );
  }
}

class AltaMasivaForm extends StatefulWidget {
  AltaMasivaForm({Key key}) : super(key: key);

  @override
  _AltaMasivaFormState createState() => _AltaMasivaFormState();
}

class _AltaMasivaFormState extends State<AltaMasivaForm> {
  String dropdownValue = 'Cursos';
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Color(0xFFFB6107),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Alta Masiva',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: DropdownButton<String>(
                value: dropdownValue,
                icon: Icon(Icons.arrow_downward),
                iconSize: 24,
                elevation: 16,
                style: TextStyle(color: Colors.deepPurple),
                underline: Container(
                  height: 2,
                  color: Colors.deepPurpleAccent,
                ),
                onChanged: (String newValue) {
                  setState(() {
                    dropdownValue = newValue;
                  });
                },
                items: <String>[
                  //'Asignacion Docentes',
                  //'Asignacion Estudiantes',
                  'Cursos',
                  'Usuarios'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: UploadButton(type: dropdownValue),
            )
          ],
        ),
      ),
    );
  }
}
