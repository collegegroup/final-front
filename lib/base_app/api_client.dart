import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:sapio_admin/base_app/user_credentials_data_type.dart';
import 'package:sapio_admin/data_types/book_element_dataType.dart';
import 'package:sapio_admin/data_types/course_dataType.dart';
import 'package:sapio_admin/data_types/foro_dataType.dart';
import 'package:sapio_admin/data_types/user_dataType.dart';
import 'package:sapio_admin/data_types/task_datatype.dart';
import 'package:sapio_admin/data_types/content_dataType.dart';
import 'package:sapio_admin/data_types/task_score_dataType.dart';
import 'package:sapio_admin/data_types/message_dataType.dart';
import 'package:sapio_admin/data_types/user_score.dart';

const String baseUrl =
    "http://fremsajo.com:8080/SAPPIO-API-0.1"; // SAPPIO-API-0.1
//const String baseUrl = "http://localhost:8080";

var authHeader = {
  HttpHeaders.authorizationHeader: "Bearer ${storedUserCredentials.getToken()}",
  "Content-Type": "application/json"
};
// var client = ApiClient();

class ApiClient {
  static final ApiClient _singleton = ApiClient._internal();

  factory ApiClient() {
    return _singleton;
  }

  ApiClient._internal();

  static setAuthHeader() {
    authHeader = {
      HttpHeaders.authorizationHeader:
          "Bearer ${storedUserCredentials.getToken()}",
      "Content-Type": "application/json"
    };
  }

  Future<bool> login({String username, String password}) async {
    var response = await http.post(
      '$baseUrl/login',
      body: jsonEncode(<String, String>{
        "username": username,
        "password": password,
        "token": "",
      }),
      headers: {"Content-Type": "application/json"},
    );
    if (response.statusCode == 200) {
      if (jsonDecode(response.body)["usuario"]["tipoUsu"] == "A") {
        storedUserCredentials.setToken(jsonDecode(response.body)["token"]);
        storedUserCredentials.setName(
            jsonDecode(utf8.decode(response.body.codeUnits))["usuario"]
                    ["nombre"] ??
                "");
        storedUserCredentials.userData = User.fromJson(
            jsonDecode(utf8.decode(response.body.codeUnits))["usuario"] ?? "");
        saveUserCredentials();
        setAuthHeader();
        return true;
      }
      return false;
    }
    return false;
  }

  Future<bool> recoverPassword({String email}) async {
    var response = await http.post(
      '$baseUrl/usuarios/recuperarContra/$email',
      headers: {"Content-Type": "application/json"},
    );
    return response.statusCode == 200;
  }

  Future<List<Course>> getCourses() async {
    var response = await http.get(
      '$baseUrl/cursos/obtenerCursos',
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(utf8.decode(response.body.codeUnits));
      List<Course> courseList = List<Course>();
      for (var i = 0; i < jsonResponse.length; i++) {
        courseList.add(Course.fromJson(jsonResponse[i]));
      }
      return courseList;
    } else {
      return List<Course>();
    }
  }

  Future<List<User>> getUsersByType({String type}) async {
    var response = await http.get(
      '$baseUrl/usuarios/getUsuarioTipo/$type',
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(utf8.decode(response.body.codeUnits));
      List<User> userList = List<User>();
      for (var i = 0; i < jsonResponse.length; i++) {
        userList.add(User.fromJson(jsonResponse[i]));
      }
      return userList;
    } else {
      return List<User>();
    }
  }

  Future<bool> updElement({BookElement element}) {}

  Future<bool> addElement({BookElement element, Content content}) async {
    var response = await http.post(
      '$baseUrl/contenidos/altaContenido/${content.id}',
      body: jsonEncode(element.toStringJson()),
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> delElement({BookElement element, Content content}) async {
    var response = await http.delete(
      '$baseUrl/contenidos/bajaContenido/${content.id}/${element.id}',
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> addStudent({User user}) async {
    var response = await http.post(
      '$baseUrl/usuarios/altaEstudiante',
      body: jsonEncode(user.toJson()),
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> addTeacher({User user}) async {
    var jsonUser = user.toJson();
    jsonUser.remove('carrera');
    var response = await http.post(
      '$baseUrl/usuarios/altaDocente',
      body: jsonEncode(jsonUser),
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<List<Forum>> getForumByCourse({Course curso}) async {
    var response = await http.get(
      '$baseUrl/foros/byCurso/${curso.id}',
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(utf8.decode(response.body.codeUnits));
      List<Forum> contentList = List<Forum>();
      for (var i = 0; i < jsonResponse.length; i++) {
        Forum instance = Forum.fromJson(jsonResponse[i]);
        contentList.add(instance);
      }
      return contentList;
    }
    return List<Forum>();
  }

  Future<bool> addForum({Forum foro, Course curso}) async {
    var response = await http.post(
      '$baseUrl/foros/altaForo/${curso.id}',
      body: jsonEncode(foro.toJson()),
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> updForum({Forum foro}) async {
    var response = await http.put(
      '$baseUrl/foros/editarForo/${foro.id}',
      body: jsonEncode(foro.toJson()),
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> delForum({Forum foro, Course curso}) async {
    var response = await http.delete(
      '$baseUrl/foros/bajaForo/${foro.id}/${curso.id}',
      headers: authHeader,
    );

    return response.statusCode == 200;
  }

  Future<bool> addComentFormun({Forum foro, String message}) async {
    var body = {
      "titulo": storedUserCredentials.userData.mail,
      "contenido": message,
    };
    var response = await http.post(
      '$baseUrl/mensajes/altaMensaje/${foro.id}/${storedUserCredentials.getUserData().id}',
      body: jsonEncode(body),
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> submitTask({String url, Task tarea}) async {
    var body = {"mailUser": storedUserCredentials.userData.mail, "link": url};
    var response = await http.post(
      '$baseUrl}/entregas/altaEntrega/${tarea.id}',
      body: jsonEncode(body),
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> delMsg({Message mesage}) async {
    var response = await http.delete(
      '$baseUrl/mensajes/bajaMensaje/${mesage.id}/${mesage.idForo}',
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> addAdmin({User user}) async {
    var jsonUser = user.toJson();
    jsonUser.remove('carrera');
    var response = await http.post(
      '$baseUrl/usuarios/altaAdministrador',
      body: jsonEncode(jsonUser),
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> scoreUserCourse(
      {Course curso, String usrMail, String score}) async {
    var response = await http.post(
      '$baseUrl/inscripciones/calificacionFinal/${curso.id}/$usrMail/${double.parse(score)}',
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<List<ScoreUser>> userScoresCourse({Course course}) async {
    var response = await http.get(
      '$baseUrl/inscripciones/getCalificacionesCurso/${course.id}',
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(utf8.decode(response.body.codeUnits));
      List<ScoreUser> contentList = List<ScoreUser>();
      for (var i = 0; i < jsonResponse.length; i++) {
        ScoreUser instance = ScoreUser.fromJson(jsonResponse[i]);
        contentList.add(instance);
      }
      return contentList;
    }
    return List<ScoreUser>();
  }

  Future<bool> updUser({User user}) async {
    print(user.toJson());
    var response = await http.put(
      '$baseUrl/usuarios/editarPerfilAdmin/${user.mail}',
      body: jsonEncode(user.toJson()),
      headers: authHeader,
    );
    print(response.body);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> delUser({User user}) async {
    var response = await http.delete(
      '$baseUrl/usuarios/${user.mail}',
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> addCourse({Course course}) async {
    var response = await http.post(
      '$baseUrl/cursos/altaCurso',
      body: jsonEncode(course.toJson()),
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updCourse({Course course}) async {
    print(course.id);
    var response = await http.put(
      '$baseUrl/cursos/editarCurso/${course.id}',
      body: jsonEncode(course.toNestedJson()),
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> delContent({Content content}) async {
    var response = await http.delete(
      '$baseUrl/libros/bajaLibro/${content.id}',
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<List<BookElement>> getBookContent({Content content}) async {
    var response = await http.get(
      '$baseUrl/contenidos/libro/${content.id}',
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(utf8.decode(response.body.codeUnits));
      List<BookElement> contentList = List<BookElement>();
      for (var i = 0; i < jsonResponse.length; i++) {
        contentList.add(BookElement.fromJson(jsonResponse[i]));
      }
      return contentList;
    }
    return List<BookElement>();
  }

  Future<List<CourseEnrollment>> getCoursesByUser({User usuario}) async {
    List<CourseEnrollment> returnList = List<CourseEnrollment>();

    var response = await http.get(
      '$baseUrl/cursos/obtenerCursosByUsuario/${usuario.mail}',
      headers: authHeader,
    );
    print('response.statusCode : ${response.statusCode}');
    if (response.statusCode == 200) {
      List<Course> allCourses = await getCourses();
      var jsonResponse = json.decode(utf8.decode(response.body.codeUnits));
      List<Course> courseList = List<Course>();
      for (var i = 0; i < jsonResponse.length; i++) {
        courseList.add(Course.fromJson(jsonResponse[i]));
      }
      List<int> enrolledCoursesIds = getCousesId(courseList);
      print(enrolledCoursesIds);
      for (var curso in allCourses) {
        var nuevoItem = new CourseEnrollment();
        nuevoItem.curso = curso;
        nuevoItem.isEnrolled = enrolledCoursesIds.contains(curso.id);
        returnList.add(nuevoItem);
      }
      return returnList;
    } else {
      return returnList;
    }
  }

  Future<bool> delCourse({Course course}) async {
    var response = await http.delete(
      '$baseUrl/cursos/bajaCurso/${course.id}',
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> deEnrollUser({User user, Course course}) async {
    var body = {
      "cursoId": course.id,
      "mailUsuario": user.mail,
    };
    var response = await http.post(
      '$baseUrl/inscripciones/bajaInscripcion/',
      body: jsonEncode(body),
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> addContent({Content content, Course curso}) async {
    var response = await http.post(
      '$baseUrl/libros/altaLibro/${curso.id}',
      body: jsonEncode(content.toNestedJson()),
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> updContent({Content content, Course curso}) async {
    var response = await http.put(
      '$baseUrl/libros/editarLibro/${content.id}',
      body: jsonEncode(content.toNestedJson()),
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> enrollUser({User user, Course course}) async {
    var body = {
      "cursoId": course.id,
      "mailUsuario": user.mail,
      "Inscripcion": {
        "notaFinal": 0,
        "fechaInicial": course.fechaInicio,
        "fechaFinal": "2020-11-31",
        "horario": "17:00:00"
      }
    };
    var response = await http.post(
      '$baseUrl/inscripciones/altaInscripcion/',
      body: jsonEncode(body),
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> enrollKnownUser({String userMail, String courseId}) async {
    var body = {
      "cursoId": courseId,
      "mailUsuario": userMail,
      "Inscripcion": {
        "notaFinal": 0,
        "fechaInicial": "2020-11-31",
        "fechaFinal": "2020-11-31",
        "horario": "17:00:00"
      }
    };
    var response = await http.post(
      '$baseUrl/inscripciones/altaInscripcion/',
      body: jsonEncode(body),
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> addTask({Task task, Course curso}) async {
    var response = await http.post(
      '$baseUrl/tareas/altaTarea/${curso.id}',
      body: jsonEncode(task.toJson()),
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> updTask({Task task}) async {
    var response = await http.put(
      '$baseUrl/tareas/editarTarea/${task.id}',
      body: jsonEncode(task.toJson()),
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<bool> delTask({Task task, Course curso}) async {
    var response = await http.delete(
      '$baseUrl/tareas/bajaTarea/${task.id}/${curso.id}',
      headers: authHeader,
    );
    return response.statusCode == 200;
  }

  Future<List<Task>> getTasksByCourse({Course curso}) async {
    var response = await http.get(
      '$baseUrl/tareas/byCurso/${curso.id}',
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(utf8.decode(response.body.codeUnits));
      List<Task> contentList = List<Task>();
      for (var i = 0; i < jsonResponse.length; i++) {
        contentList.add(Task.fromJson(jsonResponse[i]));
      }
      return contentList;
    }
    return List<Task>();
  }

  Future<List<TaskScore>> getTaskScoresByTask({Task tarea}) async {
    var response = await http.get(
      '$baseUrl/entregas/byTarea/${tarea.id}',
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(utf8.decode(response.body.codeUnits));
      List<TaskScore> contentList = List<TaskScore>();
      for (var i = 0; i < jsonResponse.length; i++) {
        TaskScore instance = TaskScore.fromJson(jsonResponse[i]);
        contentList.add(instance);
      }
      return contentList;
    }
    return List<TaskScore>();
  }

  Future<bool> deEnrollStudent({User user, Course course}) async {}
  Future<List<Content>> getContentByCourse({Course curso}) async {
    var response = await http.get(
      '$baseUrl/libros/curso/${curso.id}',
      headers: authHeader,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(utf8.decode(response.body.codeUnits));
      List<Content> contentList = List<Content>();
      for (var i = 0; i < jsonResponse.length; i++) {
        contentList.add(Content.fromJson(jsonResponse[i]));
      }
      return contentList;
    }
    return List<Content>();
  }

  Future<bool> updSelfUser({User user, String pass}) async {
    bool toReturn = false;
    var response = await http.put(
      '$baseUrl/usuarios/editarPerfil/${storedUserCredentials.userData.mail}',
      body: jsonEncode(user.toNestedValidatedJson(pass)),
      headers: authHeader,
    );

    var jsonResponse = json.decode(utf8.decode(response.body.codeUnits));

    if (response.statusCode == 200) {
      if (jsonResponse["error"] == "NONE") {
        toReturn = true;
        storedUserCredentials.userData.mail = user.mail;
        storedUserCredentials.userData.nombre = user.nombre;
        storedUserCredentials.userData.direccion = user.direccion;
        storedUserCredentials.userData.documento = user.documento;
      } else {
        toReturn = false;
      }
    } else {
      toReturn = false;
    }

    return toReturn;
  }

  Future<bool> updPass({String newPass, String oldPass}) async {
    var body = {
      "mail": storedUserCredentials.userData.mail,
      "oldpass": oldPass,
      "newpass": newPass,
    };
    var response = await http.put(
      '$baseUrl/usuarios/cambiarPass/',
      body: jsonEncode(body),
      headers: authHeader,
    );
    return response.statusCode == 200;
  }
}

class JsonEncode {}

List<int> getCousesId(List<Course> cursos) {
  List<int> respuesta = List<int>();
  for (var curso in cursos) {
    respuesta.add(curso.id);
  }
  return respuesta;
}

const empty_body = {
  "notaFinal": "",
  "fechaInicial": "2020-06-01",
  "fechaFinal": "2020-11-31"
};
