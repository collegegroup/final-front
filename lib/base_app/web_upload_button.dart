import 'dart:convert';
import 'dart:html';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'api_client.dart';

class UploadButton extends StatelessWidget {
  final String type;
  const UploadButton({Key key, @required this.type}) : super(key: key);

  Future<bool> massiveUpload(String type, String file) async {
    //
    var response = await http.post(
      '$baseUrl/$type',
      body: file,
      headers: authHeader,
    );

    return response.statusCode == 200;
    /*
    var response =
        await http.MultipartRequest('POST', Uri.parse('$baseUrl/$type'));
    print(file.relativePath);
    response.files.add(await http.MultipartFile.fromPath('text', file.name));*/
  }

  Future<void> uploadFile(String file) async {
    if (type == null) {
      print("Type es null");
    } else {
      switch (type) {
        case "Usuarios":
          print("Entro a usuarios");
          massiveUpload("usuarios/altaUsuarios/csv", file);
          break;
        case "Cursos":
          print("Entro a cursos");
          massiveUpload("cursos/altaCurso/csv", file);
          break;
        case "Asignacion Estudiantes":
          print("Entro a asig estud");
          massiveUpload("inscripciones/altaMasivaInscripcion", file);
          break;
        case "Asignacion Docentes":
          print("Entro a asig docen");
          massiveUpload("inscripciones/altaMasivaDocenteCurso", file);
          break;
        default:
          print("No entro a nada: " + type);
          break;
      }
    }
  }

  Future<File> getFile() async {
    InputElement uploadInput = FileUploadInputElement();
    uploadInput.click();
    uploadInput.onChange.listen((e) {
      // read file content as dataURLs
      final files = uploadInput.files;
      if (files.length == 1) {
        final file = files[0];
        FileReader reader = new FileReader(); // reader.result
        //reader.readAsArrayBuffer(file); //Da el Uint8List
        //reader.readAsDataUrl(file); //Da el Base64 en String
        reader.onLoadEnd.listen((event) {
          print("selecciono archivo");
          uploadFile(reader.result); //reader.result);
        });
        reader.readAsDataUrl(file);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Text("Subir Archivo"),
      onPressed: () {
        getFile();
        Navigator.of(context).pop();
      },
    );
  }
}
