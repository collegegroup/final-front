import 'package:flutter/material.dart';
import 'package:sapio_admin/pages/user_listing_page.dart';

import 'master_drawer.dart';

class DesktopLandingPage extends StatefulWidget {
  DesktopLandingPage({Key key}) : super(key: key);

  @override
  _DesktopLandingPageState createState() => _DesktopLandingPageState();
}

class _DesktopLandingPageState extends State<DesktopLandingPage> {
  Widget _body = UserListingPage(
    title: 'Estudiantes',
  );
  _setBody(Widget val) {
    setState(() {
      _body = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    MasterDrawer md = new MasterDrawer(
      onElementSelected: (Widget val) => _setBody(val),
    );
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 4.0),
              child: Image.asset(
                'assets/images/LogoSappio.png',
                height: 40,
              ),
            ),
            Text('Sappio Admin'),
          ],
        ),
      ),
      body: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 300,
            color: Colors.grey[200],
            child: md,
          ),
          Expanded(
            child: _body,
          ),
        ],
      ),
    );
  }

  static onElementSelected(Widget val) {}
}
