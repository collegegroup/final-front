import 'package:flutter/material.dart';
import 'package:sapio_admin/base_app/api_client.dart';
import 'package:sapio_admin/data_types/course_dataType.dart';
import 'package:sapio_admin/elements/course_list_tile.dart';
import 'package:sapio_admin/elements/curso_form.dart';
import 'package:sapio_admin/elements/list_header.dart';
import 'package:sapio_admin/elements/usuario_form.dart';

import 'user_listing_page.dart';

class CourseListingPage extends StatefulWidget {
  final String title;
  final Function(Widget) onElementSelected;
  CourseListingPage({
    Key key,
    this.title = 'Título',
    @required this.onElementSelected,
  }) : super(key: key);

  @override
  _CourseListingPageState createState() => _CourseListingPageState();
}

class _CourseListingPageState extends State<CourseListingPage> {
  var client = ApiClient();
  final _debouncer = Debouncer(milliseconds: 500);
  List<Course> displayList = List<Course>();
  List<Course> courseList = List<Course>();

  void _filetList(String text) {
    _debouncer.run(() {
      print(text);
      List<Course> newList;
      if (text != "") {
        newList = courseList
            .where((f) => f.nombre.toLowerCase().startsWith(text.toLowerCase()))
            .toList();
      } else {
        newList = courseList;
      }

      setState(() {
        displayList = newList;
      });
      print(displayList.length);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              ListHeader(
                title: "Cursos",
              ),
              Expanded(
                child: FutureBuilder<List<Course>>(
                  future: client.getCourses(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<Course>> snapshot) {
                    if (snapshot.hasData) {
                      if (displayList.length == 0) {
                        courseList = snapshot.data;
                        displayList = courseList;
                      }
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            height: 40,
                            constraints: BoxConstraints(maxWidth: 200),
                            child: TextField(
                                decoration:
                                    InputDecoration(hintText: 'Buscar..'),
                                onChanged: (text) => _filetList(text)),
                          ),
                          Expanded(
                            child: ListView.builder(
                                shrinkWrap: true,
                                padding: const EdgeInsets.all(8),
                                itemCount: displayList.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return CourseListTileElement(
                                    curso: displayList[index],
                                    onElementSelected: (Widget val) =>
                                        widget.onElementSelected(val),
                                  );
                                }),
                          ),
                        ],
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return Center(child: CircularProgressIndicator());
                  },
                ),
              ),
            ],
          ),
          Positioned(
            right: 30,
            bottom: 30,
            child: FloatingActionButton.extended(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return _buildPopUp(widget.title, context);
                    });
              },
              label: Text('Agregar'),
              icon: Icon(Icons.add),
              backgroundColor: Color(0xFFFB6107),
            ),
          ),
        ],
      ),
    );
  }
}

Widget _buildPopUp(String title, BuildContext context) {
  switch (title) {
    case 'Cursos':
      return AlertDialog(
        content: CursoForm(),
      );
      break;
    case 'Estudiantes':
      return AlertDialog(
        content: UeserForm(type: 'estudiante'),
      );
      break;
    case 'Docentes':
      return AlertDialog(
        content: UeserForm(type: 'docente'),
      );
      break;
    case 'Administradores':
      return AlertDialog(
        content: UeserForm(type: 'administrador'),
      );
      break;
    default:
      return AlertDialog(
        content: Container(
          color: Colors.red,
          height: 200,
          width: 125,
        ),
      );
  }
}
