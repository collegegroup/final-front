import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sapio_admin/base_app/api_client.dart';
import 'package:sapio_admin/data_types/user_dataType.dart';
import 'package:sapio_admin/elements/curso_form.dart';
import 'package:sapio_admin/elements/list_header.dart';
import 'package:sapio_admin/elements/user_list_tile.dart';
import 'package:sapio_admin/elements/usuario_form.dart';

class UserListingPage extends StatefulWidget {
  final String title;
  UserListingPage({Key key, this.title = 'Título'}) : super(key: key);

  @override
  _UserListingPageState createState() => _UserListingPageState();
}

class _UserListingPageState extends State<UserListingPage> {
  final _debouncer = Debouncer(milliseconds: 500);
  List<User> displayList = List<User>();
  List<User> userList = List<User>();

  Future<List<User>> _getUser() {
    var client = ApiClient();
    switch (widget.title.toLowerCase()) {
      case "estudiantes":
        return client.getUsersByType(type: "E");
        break;
      case "docentes":
        return client.getUsersByType(type: "D");
        break;
      case "administradores":
        return client.getUsersByType(type: "A");
        break;
      default:
        return client.getUsersByType(type: "E");
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    displayList = List<User>();
  }

  @override
  void dispose() {
    super.dispose();
    _debouncer._timer.cancel();
  }

  void _filetList(String text) {
    _debouncer.run(() {
      print(text);
      List<User> newList;
      if (text != "") {
        newList = userList
            .where((f) => f.nombre.toLowerCase().startsWith(text.toLowerCase()))
            .toList();
      } else {
        newList = userList;
      }

      setState(() {
        displayList = newList;
      });
      print(displayList.length);
    });
  }

  bool _isNewList(List<User> oldl, List<User> newl) {
    if (oldl.length != newl.length) {
      return true;
    }
    if (oldl[0].mail != newl[0].mail) {
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              ListHeader(
                title: widget.title,
              ),
              Expanded(
                child: FutureBuilder<List<User>>(
                  future: _getUser(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<User>> snapshot) {
                    if (snapshot.hasData) {
                      if (displayList.length == 0 ||
                          _isNewList(userList, snapshot.data)) {
                        print("is new list");
                        userList = snapshot.data;
                        displayList = userList;
                      }
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            height: 40,
                            constraints: BoxConstraints(maxWidth: 200),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 10.0,
                                vertical: 8,
                              ),
                              child: TextField(
                                  decoration:
                                      InputDecoration(hintText: 'Buscar..'),
                                  onChanged: (text) => _filetList(text)),
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                                shrinkWrap: true,
                                padding: const EdgeInsets.all(8),
                                itemCount: displayList.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return UserListTileElement(
                                    usuario: displayList[index],
                                    type: widget.title.toLowerCase(),
                                  );
                                }),
                          ),
                        ],
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return Center(child: CircularProgressIndicator());
                  },
                ),
              ),
            ],
          ),
          Positioned(
            right: 30,
            bottom: 30,
            child: FloatingActionButton.extended(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return _buildPopUp(widget.title, context);
                    });
              },
              label: Text('Agregar'),
              icon: Icon(Icons.add),
              backgroundColor: Color(0xFFFB6107),
            ),
          ),
        ],
      ),
    );
  }
}

Widget _buildPopUp(String title, BuildContext context) {
  switch (title) {
    case 'Cursos':
      return AlertDialog(
        content: CursoForm(),
      );
      break;
    case 'Estudiantes':
      return AlertDialog(
        content: UeserForm(type: 'estudiante'),
      );
      break;
    case 'Docentes':
      return AlertDialog(
        content: UeserForm(type: 'docente'),
      );
      break;
    case 'Administradores':
      return AlertDialog(
        content: UeserForm(type: 'administrador'),
      );
      break;
    default:
      return AlertDialog(
        content: Container(
          color: Colors.red,
          height: 200,
          width: 125,
        ),
      );
  }
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}
