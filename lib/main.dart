import 'package:flutter/material.dart';

import 'base_app/app_builder.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    precacheImage(AssetImage("assets/images/LogoSappio.png"), context);
    precacheImage(
        AssetImage("assets/images/background_mountains.jpg"), context);
    precacheImage(AssetImage("assets/images/huevo.png"), context);
    precacheImage(AssetImage("assets/images/mente.png"), context);
    precacheImage(AssetImage("assets/images/transporte.png"), context);
    return MaterialApp(
      title: 'Sappio - Administrador',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: BaseApp(), //(title: 'Flutter Demo Home Page'),
    );
  }
}
